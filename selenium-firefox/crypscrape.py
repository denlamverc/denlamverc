# Branden L
# 8/26/21
# Web scraper for coinmarketcap.com

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys

firefoxOptions = Options()
firefoxOptions.add_argument("-headless")

browser = webdriver.Firefox(executable_path="./drivers/geckodriver", options=firefoxOptions)
browser.get('https://coinmarketcap.com/currencies/crypto-com-coin/')
# The link above can be swapped for the price of any crypto on coinmarketcap.com, but for now it is on crypto.com coin.

cryptoprice = browser.find_element_by_class_name('priceValue ')

print(cryptoprice.text)

browser.quit()
