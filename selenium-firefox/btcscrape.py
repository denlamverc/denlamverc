# Branden L
# Web scraper for coinmarketcap.com
# Only difference is changing the link to get the price of any coin on coinmarketcap
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys

firefoxOptions = Options()
firefoxOptions.add_argument("-headless")

browser = webdriver.Firefox(executable_path="./drivers/geckodriver", options=firefoxOptions)
browser.get('https://coinmarketcap.com/currencies/bitcoin/')

cryptoprice = browser.find_element_by_class_name('priceValue ')

print(cryptoprice.text)

browser.quit()
