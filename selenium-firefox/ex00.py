# Branden L
# 8/23/21

# First working site grabber. This python code grabs the title of the website by opening the site, printing the title to the terminal, then closing the browser. 

#

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# The executable_path argument below tells the web driver where the geckodriver binary is located, which is in my drivers directory
browser = webdriver.Firefox(executable_path="./drivers/geckodriver")

# This loads linuxhint.com on my Firefox browser
browser.get('https://www.linuxhint.com')

# browser.title is used to access the title of the website
print('Title: %s' % browser.title)

# Closes the browser
browser.quit()
