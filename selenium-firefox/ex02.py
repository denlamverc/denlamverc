# ex00, ex01, ex02 are all made by following the article: https://linuxhint.com/using_selenium_firefox_driver/

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys

firefoxOptions = Options()
firefoxOptions.add_argument("-headless")

browser = webdriver.Firefox(executable_path="./drivers/geckodriver", options=firefoxOptions)
browser.get('https://www.lipsum.com/feed/html')

lipsum = browser.find_element_by_id('lipsum')

print(lipsum.text)

browser.quit()
